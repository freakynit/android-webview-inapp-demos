package com.appengage.android.webviewtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class WebViewInDialogActivity extends Activity {
    public static final String KEY_JS_BRIDGE_CONSTANT = "Android";

    private Button btnShowWebView;
    private MyClickListener myClickListener;

    private InAppLayoutWebAppInterface inAppLayoutWebAppInterface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_webview_dummy_layout);

        myClickListener = new MyClickListener();

        initViews();
        initHandlers();
    }

    private void initViews(){
        btnShowWebView = (Button) findViewById(R.id.btnShowWebView);
        inAppLayoutWebAppInterface = new InAppLayoutWebAppInterface(this);
    }

    class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            if(v.getId() == R.id.btnShowWebView) {
                try {
                    showWebViewDialog();
                } catch(Exception e){
                    e.printStackTrace();
                }
            }
        }
    }

    private void initHandlers(){
        btnShowWebView.setOnClickListener(myClickListener);
    }

    private void showWebViewDialog() throws Exception {
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        WebView webViewInstance = new WebView(this);

        webViewInstance.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        webViewInstance.addJavascriptInterface(inAppLayoutWebAppInterface, KEY_JS_BRIDGE_CONSTANT);
        webViewInstance.getSettings().setJavaScriptEnabled(true);

        // Online content
        //webViewInstance.loadUrl("http:\\www.google.com");

        // Local content
        String layoutHtmlFileContents = getLayoutHtmlFileContents("layout1template4.html");
        layoutHtmlFileContents = convertTemplate(layoutHtmlFileContents, getTemplateData());
        webViewInstance.loadDataWithBaseURL("file:///android_asset/", layoutHtmlFileContents, "text/html", "utf-8", null);

        webViewInstance.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });

        alert.setView(webViewInstance);
        alert.show();
    }

    private String convertTemplate(String contents, Map<String, String> d){
        for (Map.Entry<String, String> entry : d.entrySet()) {
            contents = contents.replaceAll("\\{\\{"+entry.getKey()+"\\}\\}", entry.getValue());
        }

        return contents;
    }

    private String getLayoutHtmlFileContents(String filePath) throws Exception {
        StringBuilder buf = new StringBuilder();
        InputStream json = getAssets().open(filePath);
        BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));

        String str;
        while ((str = in.readLine()) != null) {
            buf.append(str);
        }

        in.close();

        return buf.toString();
    }

    private static Map getTemplateData(){
        Map<String, String> d = new HashMap<String, String>();
        d.put("title", "This is title 2");
        d.put("main-image", "MakeUp/images/minions2.jpg");
        d.put("main-cta-id", "button-id-1");
        d.put("main-cta-text", "Click me");

        return d;
    }
}
