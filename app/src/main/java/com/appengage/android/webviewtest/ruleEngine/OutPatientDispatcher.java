package com.appengage.android.webviewtest.ruleEngine;

public class OutPatientDispatcher implements ActionDispatcher {
    @Override
    public void fire() {
        // send patient to in_patient
        System.out.println("From OutPatientDispatcher");
    }
}
