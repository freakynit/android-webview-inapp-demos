package com.appengage.android.webviewtest;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class InAppLayoutWebAppInterface {
    Activity mActivity;
    Context mContext;

    InAppLayoutWebAppInterface(Activity activity) {
        this.mActivity = activity;
        this.mContext = activity.getApplicationContext();
    }

    @JavascriptInterface
    public void showToast(String toast) {
        Log.d("APP_WEBVIEW", "showToast() called");
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void closeInAppNotification() {
        Log.d("APP_WEBVIEW", "closeInAppNotification() called");
        ((Activity) mContext).finish();
    }

    @JavascriptInterface
    public void handleCta(int idx) {
        String msg = "handleCta(" + idx + ") called";
        Log.d("APP_WEBVIEW", msg);
        Toast.makeText(mContext, msg, Toast.LENGTH_SHORT).show();
    }

    @JavascriptInterface
    public void showDialog(String dialogMsg) {
        AlertDialog alertDialog = new AlertDialog.Builder(mContext).create();
        alertDialog.setTitle("JS triggered Dialog");
        alertDialog.setMessage(dialogMsg);
        alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Toast.makeText(mContext, "Dialog dismissed!", Toast.LENGTH_SHORT).show();
            }
        });

        alertDialog.show();
    }

    @JavascriptInterface
    public void moveToNextScreen() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("Are you sure you want to leave to next screen?");
        alertDialog.setPositiveButton("YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        mContext.startActivity(new Intent(mActivity, ChennaiIntent.class));
                    }
                });

        alertDialog.setNegativeButton("NO",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        alertDialog.show();
    }
}
