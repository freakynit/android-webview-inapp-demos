package com.appengage.android.webviewtest.ruleEngine;

public class NullActionDispatcher implements ActionDispatcher {
    @Override
    public void fire() {
        // send patient to in_patient
        System.out.println("From NullActionDispatcher");
    }
}

