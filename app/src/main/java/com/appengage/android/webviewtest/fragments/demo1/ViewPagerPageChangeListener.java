package com.appengage.android.webviewtest.fragments.demo1;

import android.support.v4.view.ViewPager;
import android.util.Log;

public class ViewPagerPageChangeListener implements ViewPager.OnPageChangeListener {
    @Override
    public void onPageSelected(int position) {
        Log.d("fragment", "onPageSelected. Selected page position: " + position);
    }

    // This method will be invoked when the current page is scrolled
    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        //Log.d("fragment", "onPageScrolled");
    }

    // Called when the scroll state changes:
    // SCROLL_STATE_IDLE, SCROLL_STATE_DRAGGING, SCROLL_STATE_SETTLING
    @Override
    public void onPageScrollStateChanged(int state) {
        Log.d("fragment", "onPageScrollStateChanged: " + state);
    }
}
