package com.appengage.android.webviewtest.ruleEngine;

public interface ActionDispatcher {
    void fire();
}
