package com.appengage.android.webviewtest;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.appengage.android.webviewtest.fragments.demo1.FragmentViewPagerActivity;

public class MainActivity extends Activity {
    private Button btnLayout1Template1, btnLayout1Template2, btnLayout1Template3, btnLayout1Template4, btnLayout1Template5, btnLayout2Template1, btnLayout2Template2;
    private MyClickListener myClickListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnLayout1Template1 = (Button) findViewById(R.id.btnLayout1Template1);
        btnLayout1Template2 = (Button) findViewById(R.id.btnLayout1Template2);
        btnLayout1Template3 = (Button) findViewById(R.id.btnLayout1Template3);
        btnLayout1Template4 = (Button) findViewById(R.id.btnLayout1Template4);
        btnLayout1Template5 = (Button) findViewById(R.id.btnLayout1Template5);

        btnLayout2Template1 = (Button) findViewById(R.id.btnLayout2Template1);
        btnLayout2Template2 = (Button) findViewById(R.id.btnLayout2Template2);

        myClickListener = new MyClickListener();
        initHandlers();
    }

    class MyClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            Intent i = new Intent(MainActivity.this, InAppLayoutActivity.class);

            if(v.getId() == R.id.btnLayout1Template1) {
                i.putExtra(InAppLayoutActivity.KEY_LAYOUT_INDEX, 0);
                i.putExtra(InAppLayoutActivity.KEY_TEMPLATE_INDEX, 0);
            } else if(v.getId() == R.id.btnLayout1Template2) {
                i.putExtra(InAppLayoutActivity.KEY_LAYOUT_INDEX, 0);
                i.putExtra(InAppLayoutActivity.KEY_TEMPLATE_INDEX, 1);
            } else if(v.getId() == R.id.btnLayout1Template3) {
                i.putExtra(InAppLayoutActivity.KEY_LAYOUT_INDEX, 0);
                i.putExtra(InAppLayoutActivity.KEY_TEMPLATE_INDEX, 2);
            } else if(v.getId() == R.id.btnLayout1Template4) {
                i = new Intent(MainActivity.this, Layout1Template4Activity.class);
            } else if(v.getId() == R.id.btnLayout1Template5) {
                i = new Intent(MainActivity.this, WebViewInDialogActivity.class);
            } else if(v.getId() == R.id.btnLayout2Template1) {
                i.putExtra(InAppLayoutActivity.KEY_LAYOUT_INDEX, 1);
                i.putExtra(InAppLayoutActivity.KEY_TEMPLATE_INDEX, 0);
            } else if(v.getId() == R.id.btnLayout2Template2) {
                i = new Intent(MainActivity.this, FragmentViewPagerActivity.class);
            } else {
                i.putExtra(InAppLayoutActivity.KEY_LAYOUT_INDEX, 0);
                i.putExtra(InAppLayoutActivity.KEY_TEMPLATE_INDEX, 0);
            }

            startActivity(i);
        }
    }

    private void initHandlers(){
        btnLayout1Template1.setOnClickListener(myClickListener);
        btnLayout1Template2.setOnClickListener(myClickListener);
        btnLayout1Template3.setOnClickListener(myClickListener);
        btnLayout1Template4.setOnClickListener(myClickListener);
        btnLayout1Template5.setOnClickListener(myClickListener);

        btnLayout2Template1.setOnClickListener(myClickListener);
        btnLayout2Template2.setOnClickListener(myClickListener);
    }
}
