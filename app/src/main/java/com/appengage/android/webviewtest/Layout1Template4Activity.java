package com.appengage.android.webviewtest;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

public class Layout1Template4Activity extends Activity {
    public static final Boolean FLAG_INTERNAL = true;
    public static final String KEY_JS_BRIDGE_CONSTANT = "Android";

    private InAppLayoutWebAppInterface inAppLayoutWebAppInterface;
    private WebView webViewInstance;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout1);

        initViews();
        initHandlers();
    }

    private void initViews(){
        webViewInstance = (WebView)findViewById(R.id.webkit);
        inAppLayoutWebAppInterface = new InAppLayoutWebAppInterface(this);

        try {
            initWebView();
        } catch(Exception e){
            e.printStackTrace();
        }
    }

    private void initHandlers(){

    }

    private void initWebView() throws Exception {
        Log.d("INAPP", "Layout data = " + loadAssetTextAsString(Layout1Template4Activity.this, "layout1template4.html"));
        webViewInstance.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        webViewInstance.addJavascriptInterface(inAppLayoutWebAppInterface, KEY_JS_BRIDGE_CONSTANT);
        webViewInstance.getSettings().setJavaScriptEnabled(true);
        String layoutHtmlFileContents = getLayoutHtmlFileContents("layout1template4.html");
        layoutHtmlFileContents = convertTemplate(layoutHtmlFileContents, getTemplateData());
        webViewInstance.loadDataWithBaseURL("file:///android_asset/", layoutHtmlFileContents, "text/html", "utf-8", null);
    }

    private String convertTemplate(String contents, Map<String, String> d){
        for (Map.Entry<String, String> entry : d.entrySet()) {
            contents = contents.replaceAll("\\{\\{"+entry.getKey()+"\\}\\}", entry.getValue());
        }

        return contents;
    }

    private String getLayoutHtmlFileContents(String filePath) throws Exception {
        StringBuilder buf = new StringBuilder();
        InputStream json = getAssets().open(filePath);
        BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));

        String str;
        while ((str = in.readLine()) != null) {
            buf.append(str);
        }

        in.close();

        return buf.toString();
    }

    private String loadAssetTextAsString(Context context, String name) {
        BufferedReader in = null;
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = context.getAssets().open(name);
            in = new BufferedReader(new InputStreamReader(is));

            String str;
            boolean isFirst = true;
            while ( (str = in.readLine()) != null ) {
                if (isFirst)
                    isFirst = false;
                else
                    buf.append('\n');
                buf.append(str);
            }
            return buf.toString();
        } catch (IOException e) {
            Log.e("Exception", "Error opening asset " + name);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e("Exception", "Error closing asset " + name);
                }
            }
        }

        return null;
    }

    public static void main(String[] args) throws IOException {
        System.out.println(System.getProperty("user.dir"));
        File f = new File(System.getProperty("user.dir") + "/layout1template4.html");

        try {
            BufferedReader br = new BufferedReader(new FileReader(f));
            try {
                StringBuilder sb = new StringBuilder();
                String line = br.readLine();

                while (line != null) {
                    sb.append(line);
                    sb.append("/");
                    line = br.readLine();
                }

                String everything = sb.toString();
                //System.out.println(everything.replaceAll("\\{\\{[a-zA-Z0-9]+\\}\\}", "hello"));
                Map<String, String> d = getTemplateData();
                for (Map.Entry<String, String> entry : d.entrySet()) {
                    System.out.println(entry.getKey() + "/" + entry.getValue());
                    everything = everything.replaceAll("\\{\\{"+entry.getKey()+"\\}\\}", entry.getValue());
                }
                System.out.println(everything);
            } catch (IOException e) {
                e.printStackTrace();
            } catch(Exception e){
                e.printStackTrace();
            } finally {
                br.close();
            }
            System.out.println(f.exists());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private static Map getTemplateData(){
        Map<String, String> d = new HashMap<String, String>();
        d.put("title", "This is title 2");
        d.put("main-image", "MakeUp/images/minions2.jpg");
        d.put("main-cta-id", "button-id-1");
        d.put("main-cta-text", "Click me");

        return d;
    }
}
