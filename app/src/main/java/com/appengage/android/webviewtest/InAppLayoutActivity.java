package com.appengage.android.webviewtest;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class InAppLayoutActivity extends Activity {
    public static final Boolean FLAG_INTERNAL = true;

    public static final String KEY_JS_BRIDGE_CONSTANT = "Android";
    public static final String PREFIX_LAYOUT = "layout";
    public static final String PREFIX_TEMPLATE = "template";

    public static final String KEY_LAYOUT_INDEX = "layoutIndex";
    public static final String KEY_TEMPLATE_INDEX = "templateIndex";

    private InAppLayoutWebAppInterface inAppLayoutWebAppInterface;
    private WebView webViewInstance;
    private Intent receivedIntent;
    private Integer layoutIndex = 0;
    private Integer templateIndex = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        receivedIntent = getIntent();
        layoutIndex = receivedIntent.getIntExtra(KEY_LAYOUT_INDEX, 0);
        templateIndex = receivedIntent.getIntExtra(KEY_TEMPLATE_INDEX, 0);
        switch(layoutIndex) {
            case 0:
                setContentView(R.layout.layout1);
                break;

            case 1:
                setContentView(R.layout.layout2);
                break;

            default:
                setContentView(R.layout.layout1);
                break;
        }

        initViews();
        initHandlers();
    }

    private void initViews(){
        webViewInstance = (WebView)findViewById(R.id.webkit);
        inAppLayoutWebAppInterface = new InAppLayoutWebAppInterface(this);

        initWebView();
    }

    private void initHandlers(){

    }

    private void initWebView(){
        Log.d("INAPP", "Layout data = " + loadAssetTextAsString(InAppLayoutActivity.this, "layout1template4.html"));
        webViewInstance.getSettings().setJavaScriptEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        webViewInstance.addJavascriptInterface(inAppLayoutWebAppInterface, KEY_JS_BRIDGE_CONSTANT);
        webViewInstance.getSettings().setAppCacheMaxSize(5 * 1024 * 1024); // 5MB
        webViewInstance.getSettings().setAppCachePath(getApplicationContext().getCacheDir().getAbsolutePath());
        webViewInstance.getSettings().setAllowFileAccess(true);
        webViewInstance.getSettings().setAppCacheEnabled(true);
        webViewInstance.getSettings().setJavaScriptEnabled(true);
        webViewInstance.getSettings().setCacheMode(WebSettings.LOAD_DEFAULT); // load online by default
        if ( !isNetworkAvailable() ) { // loading offline
            webViewInstance.getSettings().setCacheMode( WebSettings.LOAD_CACHE_ELSE_NETWORK );
        }

        if(templateIndex != 2) {
            try {
                String layoutHtmlFileContents = getLayoutHtmlFileContents(getHtmlfileName());
                webViewInstance.loadDataWithBaseURL("file:///android_asset/", layoutHtmlFileContents, "text/html", "utf-8", null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //webViewInstance.loadUrl("http://feedback.webengage.com/f/aa131abd");
            webViewInstance.setWebViewClient(new WebViewClient() {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    view.loadUrl(url);
                    return true;
                }
            });
            webViewInstance.loadUrl("http://feedback.webengage.com/f/311c463b");
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService( CONNECTIVITY_SERVICE );
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private String getHtmlfileName(){
        String filePath = "";
        String fileName = "layout" + ++layoutIndex + "template" + ++templateIndex + ".html";

        if(FLAG_INTERNAL == true) {
            filePath = fileName;
        } else {
            String baseDir = Environment.getExternalStorageDirectory().getAbsolutePath();
            filePath = baseDir + "/dev/inapp/" + fileName;
        }

        Log.d("INAPP", "filePath = " + filePath);
        return filePath;
    }

    private String getLayoutHtmlFileContents(String filePath) throws Exception {
        StringBuilder buf = new StringBuilder();
        InputStream json = getAssets().open(filePath);
        BufferedReader in = new BufferedReader(new InputStreamReader(json, "UTF-8"));

        String str;
        while ((str = in.readLine()) != null) {
            buf.append(str);
        }

        in.close();

        return buf.toString();
    }

    private String loadAssetTextAsString(Context context, String name) {
        BufferedReader in = null;
        try {
            StringBuilder buf = new StringBuilder();
            InputStream is = context.getAssets().open(name);
            in = new BufferedReader(new InputStreamReader(is));

            String str;
            boolean isFirst = true;
            while ( (str = in.readLine()) != null ) {
                if (isFirst)
                    isFirst = false;
                else
                    buf.append('\n');
                buf.append(str);
            }
            return buf.toString();
        } catch (IOException e) {
            Log.e("Exception", "Error opening asset " + name);
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (IOException e) {
                    Log.e("Exception", "Error closing asset " + name);
                }
            }
        }

        return null;
    }
}
