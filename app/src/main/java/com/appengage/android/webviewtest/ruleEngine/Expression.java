package com.appengage.android.webviewtest.ruleEngine;

import java.util.Map;

public interface Expression {
    boolean interpret(final Map<String, ?> bindings);
}
