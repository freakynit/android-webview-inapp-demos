package com.appengage.android.webviewtest.fragments.demo1;

import android.os.Bundle;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.appengage.android.webviewtest.R;

public class FragmentViewPagerActivity extends AppCompatActivity {
    FragmentPagerAdapter adapterViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewpager_with_fragment_adapter);

        adapterViewPager = new MyFragmentPagerAdapter(getSupportFragmentManager());

        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        vpPager.addOnPageChangeListener(new ViewPagerPageChangeListener());
        vpPager.setAdapter(adapterViewPager);
    }
}
