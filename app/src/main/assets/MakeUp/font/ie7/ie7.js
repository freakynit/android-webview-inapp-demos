/* To avoid CSS expressions while still supporting IE 7 and IE 6, use this script */
/* The script tag referencing this file must be placed before the ending body tag. */

/* Use conditional comments in order to target IE 7 and older:
	<!--[if lt IE 8]><!-->
	<script src="ie7/ie7.js"></script>
	<!--<![endif]-->
*/

(function() {
	function addIcon(el, entity) {
		var html = el.innerHTML;
		el.innerHTML = '<span style="font-family: \'mdb\'">' + entity + '</span>' + html;
	}
	var icons = {
		'ae_appengage': '&#xe602;',
		'ae_add': '&#xe601;',
		'ae_landingpage': '&#xe60a;',
		'ae_segment': '&#xe60c;',
		'ae_event': '&#xe60d;',
		'ae_user_analytics': '&#xe800;',
		'ae_attribution': '&#xe801;',
		'ae_settings': '&#xe614;',
		'ae_configuration': '&#xe629;',
		'ae_sdk': '&#xe62a;',
		'ae_star': '&#xe615;',
		'ae_circularadd': '&#xe604;',
		'ae_circularminus': '&#xe620;',
		'ae_segcheck': '&#xe628;',
		'ae_bigcheck': '&#xe61c;',
		'ae_cross': '&#xe605;',
		'ae_fav': '&#xe607;',
		'ae_hamburger': '&#xe608;',
		'ae_home': '&#xe609;',
		'ae_logout': '&#xe60b;',
		'ae_more': '&#xe60e;',
		'ae_back': '&#xe806;',
		'ae_next': '&#xe60f;',
		'ae_notification': '&#xe610;',
		'ae_play': '&#xe611;',
		'ae_push': '&#xe612;',
		'ae_search': '&#xe613;',
		'ae_state': '&#xe616;',
		'ae_stats': '&#xe617;',
		'ae_survey': '&#xe618;',
		'ae_user': '&#xe619;',
		'ae_droid': '&#xe622;',
		'ae_apple': '&#xe62b;',
		'ae_menu_dash': '&#xe627;',
		'ae_segmentation_l': '&#xe61a;',
		'ae_segmentation_t': '&#xe61b;',
		'ae_segmentation_s': '&#xe626;',
		'ae_ab': '&#xe600;',
		'ae_check': '&#xe603;',
		'ae_cross_thin': '&#xe623;',
		'ae_upload': '&#xe624;',
		'ae_download': '&#xe606;',
		'ae_great': '&#xe625;',
		'ae_error': '&#xe61d;',
		'ae_alert': '&#xe61e;',
		'ae_info': '&#xe61f;',
		'ae_trash': '&#xe621;',
		'ae_calender': '&#xe62c;',
		'ae_email': '&#xe802;',
		'ae_geo': '&#xe803;',
		'ae_last_seen': '&#xe804;',
		'ae_time': '&#xe805;',
		'ae_back_thin': '&#xe807;',
		'ae_next_thin': '&#xe808;',
		'0': 0
		},
		els = document.getElementsByTagName('*'),
		i, c, el;
	for (i = 0; ; i += 1) {
		el = els[i];
		if(!el) {
			break;
		}
		c = el.className;
		c = c.match(/ae_[^\s'"]+/);
		if (c && icons[c[0]]) {
			addIcon(el, icons[c[0]]);
		}
	}
}());
