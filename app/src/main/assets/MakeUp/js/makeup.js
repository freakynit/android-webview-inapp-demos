var MakeUp = (function (MakeUp) {
    function init() {
        // _enableAllSelectFields();
        // _enableAllTextareasWithCounter();
        //_enableAllTooltips();
        // _enableAllToggles();
        _enableAccessibility();
        //_enableAllCollapsibleSections();
        // _enableAllTabControls();
        _enableAllButtonGroups();
    }

    function initAll() {
        _enableAllSelectFields();
        _enableAllTextareasWithCounter();
        _enableAllTooltips();
        _enableAllToggles();
        _enableAccessibility();
        _enableAllCollapsibleSections();
        _enableAllTabControls();
        _enableAllButtonGroups();
    }

    //------------------------Custom select field code starts---------------------

    function _enableAllSelectFields() {
        $('div.select').each(function () {
            enableThisSelectField(this);
        });
    }

    function enableThisSelectField(selectField) {
        var $this = $(selectField);

        //by default first item will be selected
        $('div.select-text', $this).html($('ul.select-menu li.select-option:first', $this).html());
        //assign the same value as 'value' to the custom select hidden input
        $('input.select-value', $this).val($('ul.select-menu li.select-option:first', $this).attr('value'));
        //add select class to that option
        $('ul.select-menu li.select-option:first', $this).addClass('selected');

        $this.on('close-select-menu', closeSelectMenu);

        $this.on('open-select-menu', openSelectMenu);

        $('.select-btn', $this).on('click', function () {
            var menu = $(this).parent().find('ul.select-menu');
            if (menu.attr('aria-hidden') === 'true') {
                $(this).parent().trigger('open-select-menu');
            }
            else {
                $(this).parent().trigger('close-select-menu');
            }
            // return false so that page is not reloaded in case the custom select is inside a form.
            return false;
        });

        $('li.select-option', $this).on('set-select-value', _setSelectValue);
        $('li.select-option', $this).on('click', function () {
            $(this).trigger('set-select-value');
        });
        $('li.select-option', $this).on('focus', function () {
            $(this).keypress(function (e) {
                if (e.which == 13) {
                    $(this).trigger('set-select-value');
                    return false;
                }
            });
        });

        var buttonWidth = $this.find('.select-btn').width();
        var listWidth = $this.find('.select-menu').width();
        if(buttonWidth < listWidth){
            $this.find('.select-inner').css('width', listWidth);
        }
        $this.find('.select-menu').css('width', '100%');
    }

    var docClickForSelect = function (e) {
        var container = $(".select");

        if (!container.is(e.target) && container.has(e.target).length === 0) // ... nor a descendant of the container
        {
//            if (container.find('ul.select-menu').attr('aria-hidden') === 'false') {
            container.trigger('close-select-menu');
//            }
        }
    }

    $(document.body).on("click", ".select", function () {
        var currentId = $(this).attr('id');
        $('.select').each(function () {
            if ($(this).attr('id') != currentId) {
                $(this).trigger('close-select-menu');
            }
        })
    });

    function getSelectValue(select) {
        var text = $(select).parent().siblings('.select-btn').find('div.select-text').html();
        var val = $(select).parent().siblings('input.select-value').val();
        return(
        {"text": text, "value": val}
            );
    }

    function _setSelectValue() {
        $(this).parent().trigger('close-select-menu');
        $(this).parent().siblings('.select-btn').find('div.select-text').html($(this).html());
        $(this).parent().siblings('input.select-value').val($(this).attr('value'));
        $(this).parent().find('.selected').removeClass('selected');
        $(this).addClass('selected');
        $(this).parent().siblings('input.select-value').trigger('change');
    }

    function openSelectMenu() {
        var menu = $(this).find('ul.select-menu');
        if ($(this).hasClass('select')) {
            $(this).find('.select-btn .select-icon').html('&#x25B4;');

            // update the aria-expanded attribute of the region
            menu.attr('aria-expanded', 'true');

            // update the aria-hidden attribute of the region
            menu.attr('aria-hidden', 'false');

            // move focus to the first item in list
            menu.find('.select-option').attr("tabindex", "0");
            (menu.find('.select-option')[0]).focus();
        }
        $(document).bind('click', docClickForSelect);
        return false;
    }

    function closeSelectMenu() {
        var menu = $(this).find('ul.select-menu');
        if ($(this).hasClass('select')) {
            $(this).find('.select-btn .select-icon').html('&#x25BE;');

            // update the aria-expanded attribute of the region
            menu.attr('aria-expanded', 'false');

            // update the aria-hidden attribute of the region
            menu.attr('aria-hidden', 'true');

            // move focus to the first item in list
            menu.find('.select-option').attr("tabindex", "-1");
            $(this).find('.select-btn').focus();
        }
        $(document).unbind('click', docClickForSelect);
        return false;
    }

    //----------------------Custom select field code ends------------------------


    //----------------------Custom textarea with counter code ends------------------------

    function _enableAllTextareasWithCounter() {
        $('div.textarea-with-counter').each(function () {
            enableThisTextarea(this);
        });
    }

    function enableThisTextarea(textarea) {
        var $this = $(textarea);
        var txtarea = $this.find('textarea');
        var counter = $this.find('.counter');
        //initialize with data-length
        var max = 0;
        var unit_plural = "", unit_singular = "";

        if(txtarea.attr('data-counting-unit') === 'byte'){
            unit_plural = 'bytes';
            unit_singular = 'byte';
            max = txtarea.attr('data-length') - unescape(encodeURI(txtarea.val())).length;
        }
        else{
            unit_plural = 'characters';
            unit_singular = 'character';
            max = txtarea.attr('data-length') - txtarea.val().length;
        }

        if(max === 1){
            counter.text('You have ' + max + ' ' + unit_singular + ' left');
        }
        else{
            counter.text('You have ' + max + ' ' + unit_plural + ' left');
        }

        $this.keyup(function (e) {
            var $target = $(e.target);
            var left = 0;

            if(txtarea.attr('data-counting-unit') === 'byte'){
                left = txtarea.attr('data-length') - unescape(encodeURI(txtarea.val())).length;
            }
            else{
                left = txtarea.attr('data-length') - txtarea.val().length;
            }

            if (left < 0) {
                // Maximum exceeded
                $target.val($target.val().substring(0, max));
                left = 0;
            }
            else if(left === 0){
                e.preventDefault();
            }

            //change bytes left as and when a character is typed
            if(left === 1){
                counter.text('You have ' + left + ' ' + unit_singular + ' left');
            }
            else{
                counter.text('You have ' + left + ' ' + unit_plural + ' left');
            }
        });
    }

    //----------------------Custom textarea with counter code ends------------------------


    //----------------------Custom tooltip code ends------------------------

    function _enableAllTooltips() {
        $('[data-tooltip-trigger="click"]').each(function () {
            $(this).on('click', function () {
                showTooltip($(this));
            });
        });
        $('[data-tooltip-trigger="hover"]').each(function () {
            $(this).on('mouseenter', function (e) {
                showTooltip($(this));
            });
        });
    }

    var docClickOnToolTip = function (e) {
        var tooltip = $(e.data.tooltip);
        if (!tooltip.is(e.target) && tooltip.has(e.target).length === 0) // ... nor a descendant of the container
        {
            if (tooltip.css('display') != 'none') {
                hideTooltip(tooltip);
            }
        }
    }

    function enableThisTooltip(target, tooltip) {
        var $target = $(target);
        var trigger = $target.attr('data-tooltip-trigger');
        if (trigger === 'click') {
            $target.on('click', function () {
                showTooltip($target, tooltip);
            });
        }
        else if (trigger === 'hover') {
            $target.on('mouseenter', function (e) {
                showTooltip($target);
            });
        }
        ;
    }

    function hideTooltip(tooltip) {
        removeOverlay();
        var tt = tooltip || '.tooltip';
        $(tt).hide();
        $(document).unbind('click', docClickOnToolTip);
    }


    function showTooltip($target, tooltip) {
        var tooltip = $(tooltip).find('.tooltip-body'), beak = false;
        if ($target.attr('data-tooltip')) {
            tooltip.html($target.attr('data-tooltip'));
        }
        if ($target.attr('data-beak')) {
            beak = true;
            tooltip.attr('data-beak', $target.attr('data-beak'));
        }
        tooltip.css({left: _getXPosition($target, tooltip, beak), top: _getYPosition($target, tooltip, beak)});
        addOverlay();
        tooltip.show();
        setTimeout(function () {
            $(document).bind('click', {'tooltip': tooltip}, docClickOnToolTip);
        }, 500);
    }

    function _getXPosition($sourceElement, $tooltip, beak) {
        var tooltip_x = $sourceElement.offset().left + $sourceElement.width() / 2 - $tooltip.width() / 2 + 5;
        var x_overflow_flip = "true";
        if ($tooltip.attr('data-x-overflow-flip')) {
            x_overflow_flip = $tooltip.attr('data-x-overflow-flip');
        }

        var $immediateRelativeParents = $tooltip.parents('*').filter(function () {
            var curMarginLeft = parseInt($(this).css('margin-left'));
            return ($(this).css("position") === 'relative' || $(this).css("position") === 'static') && (curMarginLeft > 0);
        });

        var largestParentMarginLeft = 0;
        $immediateRelativeParents.each(function () {
            var curLeft = parseInt($(this).css('margin-left'));
            largestParentMarginLeft = curLeft > largestParentMarginLeft ? curLeft : largestParentMarginLeft;
        });

        var lefttooltipOverflow = (tooltip_x + $tooltip.width());
        if ((lefttooltipOverflow > 0) && (lefttooltipOverflow > $(window).width())) {// goes out of screen on right
            if (x_overflow_flip === "true") {
                tooltip_x = $sourceElement.offset().left + $sourceElement.width() / 2 - $tooltip.width();
                if (!beak) {
                    $tooltip.attr('data-beak', 'right');
                }
            } else {
            }
        }
        else if (tooltip_x < 0) {// goes out of screen on left
            if (x_overflow_flip === "true") {
                tooltip_x = $sourceElement.offset().left + $sourceElement.width() / 2 + largestParentMarginLeft - 15;
                if (!beak) {
                    $tooltip.attr('data-beak', 'left');
                }
            } else {
            }
        }
        else {// inside screen
            tooltip_x = $sourceElement.offset().left + $sourceElement.width() / 2 - $tooltip.width() / 2 - largestParentMarginLeft;
        }
        // }

        return tooltip_x;
    };

    function _getYPosition($sourceElement, $tooltip, beak) {
        var sourceElementTop = $sourceElement.offset().top - $(window).scrollTop();
        var tooltip_y = sourceElementTop + $sourceElement.height() + 15;
        var y_overflow_flip = "true";
        if ($tooltip.attr('data-y-overflow-flip')) {
            y_overflow_flip = $tooltip.attr('data-y-overflow-flip');
        }
        var tooltip_beak = "top";

        var tooltipBottom = sourceElementTop + $tooltip.height() + 15;
        var windowBottom = $(window).height();
        var verticalOverflow = tooltipBottom - windowBottom;

        if (verticalOverflow > 0 && sourceElementTop - $tooltip.height() > 0) {
            if (y_overflow_flip) {
                tooltip_y = sourceElementTop - $tooltip.height() - $sourceElement.height() - 10;
                tooltip_beak = "bottom";
            }
            else {
            }
        }
        else {
            tooltip_beak = "top";
        }

        if (!beak) {
            if (($tooltip.attr('data-beak') === 'left') || ($tooltip.attr('data-beak') === 'right')) {
                $tooltip.attr('data-beak', $tooltip.attr('data-beak') + '_' + tooltip_beak);
            }
            else {
                $tooltip.attr('data-beak', tooltip_beak);
            }
        }

        return tooltip_y;
    };

    function addOverlay() {
        $('.tooltip-overlay').show();
    }

    function removeOverlay() {
        $('.tooltip-overlay').hide();
    }

    //----------------------Custom tooltip code ends------------------------

    //----------------------Custom toggle code ends------------------------

    function _enableAllToggles() {
        $('div.toggle').each(function () {
            enableThisToggle(this);
        });
    }

    function enableThisToggle(toggle) {
        $(toggle).on('click', function () {
            toggleValues($(toggle));
        });
    }

    function _updateToggle($toggle) {
        var toggleVal = $toggle.find('.toggle-value').val();
        if (toggleVal === 'true') {
            $toggle.find('.toggle-text').html('ON');
            $toggle.attr('data-state', 'ON');
        }
        else if (toggleVal === 'false') {
            $toggle.find('.toggle-text').html('OFF');
            $toggle.attr('data-state', 'OFF');
        }
    }

    function toggleValues($toggle) {
        var toggleVal = $toggle.find('.toggle-value').val();
        if (toggleVal === 'true') {
            $toggle.find('.toggle-value').val('false');
        }
        else if (toggleVal === 'false') {
            $toggle.find('.toggle-value').val('true');
        }

        _updateToggle($toggle);
    }

    //----------------------Custom toggle code ends------------------------

    //----------------------Accessibility code------------------------

    function _enableAccessibility() {
        $("*[tabindex=0]").each(function () {
            $(this).keypress(function (e) {
                if (e.which == 13) {
                    $(this).trigger('click');
                    return false;
                }
            });
        });
    }

    //----------------------Accessibility code ends------------------------

    //----------------------Collapsible Section code------------------------


    function enableCollapsibleSections(parent) {
        $(parent).on('click', '.collapsible-section.clickable .collapsible-section-header', function () {
            var _parent = $(this).closest('.collapsible-section.clickable');

            if (_parent.hasClass('collapse')) {
                openCollapsibleSection(_parent);
            }
            else {
                closeCollapsibleSection(_parent);
            }
        });
    }

    function _enableAllCollapsibleSections() {
        $('.collapsible-section.clickable .collapsible-section-header').each(function () {
            enableThisCollapsibleSection(this);
        });
    }

    function enableThisCollapsibleSection(colSec) {
        $(colSec).on('click', function () {
            var parent = $(this).closest('.collapsible-section.clickable');
            if (parent.hasClass('collapse')) {
                openCollapsibleSection(parent);
            }
            else {
                closeCollapsibleSection(parent);
            }
        });
    }

    function setCollapsibleSectionValue(value) {
        $('.collapsible-section-header-subtitle', $(this)).html(value);
    }

    function openCollapsibleSection(sec) {
        closeCollapsibleSection(null);
        //sec.removeClass('validated').removeClass('clickable')
        $(sec).removeClass('collapse');
    }

    function closeCollapsibleSection(sec) {
        if(sec){
            $(sec).addClass('collapse');
        }
        else{
            $('.collapsible-section').addClass('collapse');
        }
    }

    //----------------------Collapsible Section ends------------------------

    //----------------------Tab Control code------------------------

    function _enableAllTabControls() {
        $('.tab-control').each(function () {
            enableThisTabControl(this);
        });
    }

    function enableThisTabControl(tabControl) {
        var $this = $(tabControl);
        var tabs = $('.tab[aria-controls]', $this);
        tabs.on('click', function (e) {
            if ($(this).hasClass('add-new-tab')) {

            }
            else {
                var oldTab = $(this).closest('.tablist').find('.tab-selected');
                oldTab.removeClass('tab-selected');
                $('#' + oldTab.attr('aria-controls')).removeClass('tabpanel-selected');
                $(this).addClass('tab-selected');
                $('#' + $(this).attr('aria-controls')).addClass('tabpanel-selected');
            }
        });
    }

    //----------------------Tab Control ends------------------------

    //----------------------Button Group code------------------------

    function _enableAllButtonGroups() {
        $('.button-group').each(function () {
            enableThisButtonGroup(this);
        });
    }

    function enableThisButtonGroup(buttonGroup) {
        var $this = $(buttonGroup);
        var buttons = $('.button', $this);
        buttons.on('click', function (e) {
            if ($(this).hasClass('is-button-selected')) {

            }
            else {
                var oldButton = $(this).closest('.button-group').find('.is-button-selected');
                oldButton.removeClass('is-button-selected');
                oldButton.attr('aria-selected', 'false');
                $(this).addClass('is-button-selected');
                $(this).attr('aria-selected', 'true');
            }
        });
    }

    //----------------------Button Group ends------------------------

    MakeUp.init = init;
    MakeUp.initAll = initAll;

    MakeUp.getSelectValue = getSelectValue;
    MakeUp.openSelectMenu = openSelectMenu;
    MakeUp.closeSelectMenu = closeSelectMenu;
    MakeUp.enableThisSelectField = enableThisSelectField;

    MakeUp.enableThisTextarea = enableThisTextarea;

    MakeUp.hideTooltip = hideTooltip;
    MakeUp.showTooltip = showTooltip;
    MakeUp.enableThisTooltip = enableThisTooltip;

    MakeUp.toggleValues = toggleValues;
    MakeUp.enableThisToggle = enableThisToggle;

    MakeUp.setCollapsibleSectionValue = setCollapsibleSectionValue;
    MakeUp.openCollapsibleSection = openCollapsibleSection;
    MakeUp.closeCollapsibleSection = closeCollapsibleSection;
    MakeUp.enableThisCollapsibleSection = enableThisCollapsibleSection;
    MakeUp.setCollapsibleSectionValue = setCollapsibleSectionValue;
    MakeUp.enableCollapsibleSections = enableCollapsibleSections;

    MakeUp.enableThisTabControl = enableThisTabControl;

    MakeUp.addOverlay = addOverlay;
    MakeUp.removeOverlay = removeOverlay;

    MakeUp.enableThisButtonGroup = enableThisButtonGroup;

    return MakeUp;
})({});
